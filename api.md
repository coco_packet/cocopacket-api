[[_TOC_]]

# Introduction

We realize our documentation is not fully there, it is a work in progress. \
Thank you for your understanding.

# HOW TO

## Authentication
For uthorization use **basic http authentication**

```
curl --user admin:password "https://_name_.client.cocopacket.com/v1/_call_"
```
- \_name\_ - your instance name
- \_call\_ - specific API call

## Authorization
CocoPacket recognizes two roles **ADMIN** or **USER**.
Recomended level is assigned to each API call.


# WORK WITH IPs

## add IPs
*PUT* `/v1/mconfig/add` _admin_|_user_  
**[json-payload]**
```json
{
    "ips": {
        "1.1.1.1": {
            "cat":    ["GROUP->SUBGROUP->", "OTHER->"],
            "desc":   "ip description",
            "fav":    false,
            "slaves": ["PRAGUE", "LONDON"],
            "report": [],
            "as":     0,
            "expire": "2020-12-01T23:50:00Z"
        },
        "8.8.8.8": {
            "cat":    ["GROUP->SUBGROUP->"],
            "desc":   "google dns",
            "fav":    false,
            "slaves": ["PRAGUE", "LONDON", "PARIS"],
            "report": [],
            "as":     0,
            "expire": "0001-01-01T00:00:00Z",
            "locked": true
        }
    }
}
```
_**MANDATORY KEYS**: ["cat", "slaves"] **OPTIONAL KEYS**: ["desc", "fav", "report", "as", "expire"]_

- "cat" - cateogory is a how your IPs are sorted/assignd in the sidepanel
- '->' - Private delimeter that separetes each level, do not use in the name of the category.
- '->' - must be also at the very END of the string.\

### curl example:
```
curl -i --user admin:password -H "Content-Type: application/json" -X PUT "http://xxx.client.cocopacket.com/v1/mconfig/add" --data '{
  "ips": {
    "1.1.1.1": {
        "cat":    ["GROUP->SUBGROUP->"],
        "desc":   "one-test",
        "slaves": ["PROBE1","PROBE2"]
    },
    "2.2.2.2": {
        "cat":    ["GROUP->SUBGROUP->"],
        "desc":   "twoo-test",
        "slaves": ["PROBE1"]
    }
  },
  "force": false
}'

```

### categories explanation:
```json
{
    "1.1.1.1": { "cat":["GROUP_1->SUBGROUP_1->"], ...},
    "2.2.2.2": { "cat":["GROUP_1->SUBGROUP_2->"], ...},
    "3.3.3.3": { "cat":["GROUP_2->"], ...}
}
```
^^^ results in:
- GROUP_1
    - SUBGROUP_1
        - 1.1.1.1
    - SUBGROUP_2
        - 2.2.2.2
- GROUP_2
    - 3.3.3.3


## delete many IPs at the same time
*PUT* `/v1/mconfig/delete` _admin_  
json-payload
```json
{
    "ips": ["8.8.8.8", "1.1.1.1"],
    "force": false
}
```

## get hash for public ip link
*GET* `/v1/linkkey/:ip`   

# WORK WITH GROUPS

## remove group (category)
*DELETE* `/v1/group/:group` _admin_  
*warning* all IPs/tests in group which not present in another group will be deleted  
operation is not recursive, so deletion of "GROUP->" will not delete "GROUP->SUBGROUP->"  

## get configuration for group
*GET* `/v1/group/:group` _admin_  

## set configuration for group
*POST* `/v1/group/:group` _admin_  
JSON-payload have same format as GET has  
fields description:
* `isPublic`  export group to public URL
* `lossThreshold`, `latencyThreshold` and `timeThreshold` are for push notifications
* `pushNotifyA` string list of push notify configurations applied to this group
* `isAutoGroup` if `true` then backned is trying to monitor that it's enough live IPs in this group and in worst case auto add them
* `agNetwork` network (like 8.8.8.0/24) or AS (like AS15169) where too lookup live IPs for this group
* `agCount` how many live IPs this group needs
* `agSlaves` on which slaves add new IPs
* `slavesThresholds` object(map) with string keys (slave names) and 3-field object as a value containig `lossThreshold`, `latencyThreshold` and `timeThreshold` overrides for specified slave(s) ![1.0.4-7](https://img.shields.io/static/v1?label=ver&message=1.0.4-7&color=white)

## set alert configuration for multiply groups ![1.0.4-30](https://img.shields.io/static/v1?label=ver&message=1.0.4-30&color=white)
*POST* `/v1/mgconfig/alerts` _admin_  
JSON-payload have same format as GET has  
fields description:
* `groups` string list of groups to set alert settings to
* `lossThreshold`, `latencyThreshold` and `timeThreshold` are for push notifications
* `pushNotifyA` string list of push notify configurations applied to this group
* `slavesThresholds` object(map) with string keys (slave names) and 3-field object as a value containig `lossThreshold`, `latencyThreshold` and `timeThreshold` overrides for specified slave(s) 


## rename group
*PUT* `/v1/group/:group` _admin_  
json-payload
```json
{ "newname": "NEW->" }
```

# WORK WITH PROBES _(legacy naming "SLAVE")_

## set slaves list for ip
*PUT* `/v1/slaves/:ip` _admin_  
json-payload:
```json
   ["PRAGUE", "LONDON"]
```

## get slaves list for ip/test
*GET* `/v1/slaves/:ip`   

## add / update slave
*POST* `/v1/slaves` _admin_  
json-payload:
```json
{
	"ip":    "1.1.1.1",
	"port":  1234,
	"name":  "NEW-SLAVE",
	"copy":  "LONDON",
	"update": false,
}
```
if `copy` is not empty then add to new created slave all ips/tests exist on specified existing slave  
if `update` is `true` then just update IP/port of just existing slave  

## get list of configured slaves
*GET* `/v1/slaves`   

## remove slave from system
*DELETE* `/v1/slaves?slave=XXX` _admin_  
*warning* also removes all IPs that have not other slaves

## rename slave
*PUT* `/v1/slaves` _admin_  
json-payload:
```json
{
    "oldName": "PRG-DEFAULT",
    "newName": "PRG-COGENT"
}
```

## add/remove slaves for list of ips
*PUT* `/v1/mconfig/slaves` _admin_  
json-payload
```json
{
    "ips": ["8.8.8.8", "1.1.1.1"],
    "slaves": {
      "PRAGUE": true,
      "LONDON": false
    }
}
```
rules is the same as for group-slave-add/remove

## update slaves for all IPs/tests in group
*PUT* `/v1/groupslaves/:group` _admin_  
json-payload
```json
{
  "slaves": {
      "PRAGUE": true,
      "LONDON": false
  }
  "recursive": true
}
```
all unspecified slaves will remain unchanged  
slaves with `true` value will be added to all IPs/tests in group (and subgroups in case of recursive)  
slaves with `false` values will be removed from all IPs/tests in group (and subgroups in case of recursive)  


# WORK WITH NOTIFICATIONS

## configure push notifications ![1.0.2-0](https://img.shields.io/static/v1?label=ver&message=1.0.2-0&color=white)
*PUT* `/v1/notify` _admin_  
put whole list of push notify destinations
```json
{
    "sms": {
	    "method": "POST",
	    "url": "https://sms.gateway.com/something-more..",
	    "payload": "{ some json... }",
	    "contentType": "application/json",
	    "headers": {
            "Auth": "token"
        },
	    "frequency": 60,
	    "frequencyPerIP": 300,
	    "minSlavesFailed": 1
    },
    "telegram": ...
}
```
`method` GET/PUT/POST  
`url` just a url :) 
`payload`  what to send, valid for PUT/POST  
`contentType`  usualy application/json or text/plain  
`headers`  additional HTTP headers needed
`frequency`  one notification per "frequency" secodns (for example 60 means that only one message per minute will be send), can be set to zero  
`frequencyPerIP`  one notification per "frequency" secodns for one ip (for example 300 means that only one message per 5 minutes will be send for failed ip... but other messages will be send regarding to `frequency`)  
`minSlavesFailed` ![1.0.4-6](https://img.shields.io/static/v1?label=ver&message=1.0.4-6&color=white) send message only if at least minSlavesFailed slaves reports a problem with one IP   

in `url` and `payload` any `<*IP*>`, `<*SLAVE*>`, `<*LATENCY*>`, `<*LOSS*>` and `<*GROUP*>` will be replaced with current inident values  
in case of `minSlavesFailed > 1` data from message received from last slave that triggered incident is used  
additional values `<*SCOUNT*>` (at least `<*SCOUNT*>` slaves triggered) and `<*SLAVES*>` (comma separated list of slave at the moment of event pushing) added ![1.0.4-6](https://img.shields.io/static/v1?label=ver&message=1.0.4-6&color=white)

new values: `<*PUSH*>` with name of push notify; `<*NAME*>` and `<*ALIAS*>` both contains ip alias ![1.3.2-3](https://img.shields.io/static/v1?label=ver&message=1.3.2-3&color=white)

push notifications are processes over 60-second slices of data so it's always be about 1-minute delay

## get current list of push notification configurations ![1.0.2-0](https://img.shields.io/static/v1?label=ver&message=1.0.2-0&color=white)
*GET* `/v1/notify`   

## send test notification
*GET* `/v1/notify/:type/:ip/:group?slave=XXX?&avg=10.20&loss=5&push=pushConfigurationName` _admin_  
just send testing push notification, usable for debuginh


# WORK WITH ALERTS
To setup alerts a group must have assigned notification and thresholds 
## Setup notification
[ADD/EDIT notifications] (# WORK WITH NOTIFICATIONS)
## Activate alert
set following: ([see more here](## set configuration for group))
* `lossThreshold`, `latencyThreshold` and `timeThreshold` are for push notifications
* `pushNotifyA` string list of push notify configurations applied to this group

# OPTIONS
## Email Templates

### get current password recovery mail template ![1.0.4-4](https://img.shields.io/static/v1?label=ver&message=1.0.4-4&color=white)

*GET* `/v1/config/preset`
i

### set new password recovery mail template ![1.0.4-4](https://img.shields.io/static/v1?label=ver&message=1.0.4-4&color=white)

*POST* `/v1/config/preset`
json-payload:
```json
{
    "subject": "CocoPacket password reset",
    "contentType": "text/plain",
    "body": "Your password for CocoPacket on address {URL} is reseted to {PASSWORD}"
}
```
`{URL}` and `{PASSWORD}` will be replaced with actual URL of instance and new password  
in case of empty subject/contentType/body in *PUT* call they will be filled with default values


## Config
### get configuration
*GET* `/v1/config`   

### replace whole "config" (list of test targets)
*PUT* `/v1/config` _admin_  
except JSON payload, same format as retrived with GET  

### remove one entry (ip or http test)
*DELETE* `/v1/config/:type/:ip` _admin_  
`:type` can be `ping` or `http` in stable version  

### add test or replace test configuration
*PUT* `/v1/config/:type/:ip` _admin_|_user_  
access to this API can be configured to just admin or to any user  
`:type` can be `ping` or `http` in stable version  
JSON-payload represents test params:
```json 
{
	"cat":    ["GROUP->SUBGROUP->", "OTHER->"],
	"desc":   "something about IP or HTTP test",
	"fav":    false,
	"slaves": ["PRAGUE", "LONDON"],
	"report": [],
	"as":     0,
	"expire": "2020-12-01T23:50:00Z",
    "locked": false
}
```
*warning*: in case of non-zero (`0001-01-01T00:00:00Z`) expire value ip/test will be auto-removed from system at specified date. Used for temporary add IPs for example for auto-added via support tickets.  
`as` is filled on backend side if leaved 0. If `locked` is `true` then IP can't be mofidied/removed
without `force`==`true` in API call ![1.3.2-0](https://img.shields.io/static/v1?label=ver&message=1.3.2-0&color=white).

## Status
### get slaves status
*GET* `/v1/status/slaves`   

## get backend version and license info
*GET* `/v1/status/version`   

## Maintenance

### set maintenance list ![1.0.3-6](https://img.shields.io/static/v1?label=ver&message=1.0.3-6&color=white)
*PUT* `/v1/maintenance` _admin_  
json-payload:
```json
[ "8.8.8.8", "1.1.1.1" ]
```
set list of IPs for which push notifications is *off*

### get current maintenance list ![1.0.3-6](https://img.shields.io/static/v1?label=ver&message=1.0.3-6&color=white)
*GET* `/v1/maintenance`   

# DATA

## Getting last minute status

### for one group and one location ![1.0.3-1](https://img.shields.io/static/v1?label=ver&message=1.0.3-1&color=white)
*GET* `/v1/minute/:group?slave=XXX`

### for all ips over all locations ![1.0.4-29](https://img.shields.io/static/v1?label=ver&message=1.0.4-29&color=white)
*GET* `/v1/minutedump`

## Work with single ip
all `:from` and `:to` represents unix timestamps

### Get RAW data
*GET* `/v1/get/:ip/:from/:to?slave=:slave`

### Aggregate data by period
*GET* `/v1/agr/:period/:ip/:from/:to`
* `period` is number of seconds

### Get "smoke" data
*GET* `/v1/grf/:period/:ip/:from/:to`
* `period` is number of seconds

### Get aggregated data for IP on period thru all slaves
*GET* `/v1/allinfo/:ip/:from/:to` 

### Getting dump of pre-aggregated data (fast call)
*GET* `/v1/status/storage/avgdump/:ip?slave=:slave`
returns data for last day in hour aggregations and last hour in minute aggregations

## Work with multiply IPs or slaves

### Get hour-aggregated data for last day for specified IPs on one slave
*POST* `/v1/matrix/ips?slave=:slave`
json-payload:
```json
{ "ips": ["8.8.8.8", "1.1.1.1" ] }
```

### Get hour-aggregated data for last day for specified IP on all slaves
*GET* `/v1/matrix/slaves/:ip`

### Get loss of all ips during period (may be slow if it's more than 50-100K ips on instance)
*GET* `/v1/getlost/:from/:to/:percent`

### Get loss of all ips during period having loss more then specified value (may be slow if it's more than 50-100K ips on instance)
*GET* `/v1/getlost/:from/:to`


## Get avg latency for all ips during period (may be slow if it's more than 50-100K ips on instance)
*GET* `/v1/getlat/:from/:to`

## Work with category

### Get last day hour-aggregated data for category (all IPs and slaves)
*GET* `/v1/catstats/:group?report=false`
* `report` selects only IPs selected for report in UI (or via config call)

## Get loss for all IPs in category
*GET* `/v1/getglost/:from/:to/:group`

## Get loss for all IPs in category where loss >= :percent
*GET* `/v1/getglost/:from/:to/:group/:percent`

## Selectors

### Get most lossing IPs
*GET* `/v1/mostlost/:count/:from/:to/:min/:max`
* `count` - return up to this count of IPs
* `min` - minimal % loss to return
* `max` - maximal % loss to return (for example 99 to avoid retuning totaly dead ips)
* `from`-`to` - unix timestamps fo select period

### Get ips where loss or latency changed for more then %
*GET* `/v1/getchanged/:percent/:from/:to`

### Get ips where loss changed for more then %
*GET* `/v1/lostchanged/:percent/:from/:to`

### Get ips where latency changed for more then %
*GET* `/v1/avgchanged/:percent/:from/:to`

# WORK WITH USERS

## get list of users
*GET* `/v1/users` _admin_  

## delete user
*DELETE* `/v1/users` _admin_  
form-values:
* `login`  login/email of user to delete

## add user
*PUT* `/v1/users` _admin_  
form-values:
* `login`  login/email of new user
* `passwd`  password
* `type`  only *user* or *admin* is accepted

## change password of current user
*PUT* `/v1/users/passwd` 
form-values:
* `oldpasswd`  current password
* `newpasswd`  new password

## reset password for user (force to change password)
*GET* `/v1/users/reset` _admin_  
form-values:
* `user`  email of user for password reset
