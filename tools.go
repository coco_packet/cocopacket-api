package api

import (
	"net"
	"sort"
	"strconv"
	"strings"
	"time"
)

// StrTsToTime convets string unixtime to time.Time
func StrTsToTime(s string) time.Time {
	tmp, _ := strconv.ParseInt(s, 10, 64)
	return time.Unix(tmp, 0)
}

// AvgMapToSlice used to "sort the map" as maps in golang
// are in "random" order + convert time to time.Time
func AvgMapToSlice(data map[string]*AvgItem) []*AvgItem {
	var result = make([]*AvgItem, 0, len(data))
	for _, item := range data {
		result = append(result, item)
	}

	sort.Slice(result, func(i, j int) bool {
		return result[j].TS.After(result[i].TS)
	})

	return result
}

// GetIPsInGroup returns IPs in group
func GetIPsInGroup(group string, config ConfigInfo) []string {
	result := []string{}

	if !strings.HasSuffix(group, "->") {
		group = group + "->"
	}

	for ip, d := range config.Ping.IPs {
		found := false
		for _, g := range d.Groups {
			if g == group {
				found = true
				break
			}
		}
		if found {
			result = append(result, ip)
		}
	}

	return result
}

// GetIPsInAS returns IPs in specified AS, and optional in AutoGroups with specified AS
func GetIPsInAS(as int64, alsoAutoGroups bool, config ConfigInfo) []string {
	result := []string{}
	asGroups := map[string]bool{}

	if alsoAutoGroups {
		asNetwork := "AS" + strconv.FormatInt(as, 10)
		for group, groupConfig := range config.Groups {
			if groupConfig.AGNetwork == asNetwork {
				asGroups[group] = true
			}
		}
	}

	for ip, d := range config.Ping.IPs {
		if d.AS == as {
			result = append(result, ip)
			continue
		}

		if alsoAutoGroups {
			found := false
			for _, g := range d.Groups {
				if asGroups[g] {
					found = true
					break
				}
			}
			if found {
				result = append(result, ip)
			}
		}
	}

	return result
}

// GetAutoGroupForNetwork returns auto-group name(s) for network (or AS specified like "AS12345")
func GetAutoGroupForNetwork(network string, config ConfigInfo) []string {

	result := []string{}

	for group, groupConfig := range config.Groups {
		if groupConfig.AGNetwork == network {
			result = append(result, group)
		}
	}

	return result
}

// GetIPsInCIDR returns IPs in specified CIDR, and optional in AutoGroups with specified agNetwork
func GetIPsInCIDR(cidr string, alsoAutoGroups bool, config ConfigInfo) ([]string, error) {
	result := []string{}
	autoGroups := map[string]bool{}

	_, pCIDR, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, err
	}

	if alsoAutoGroups {
		for group, groupConfig := range config.Groups {
			if groupConfig.AGNetwork == cidr {
				autoGroups[group] = true
			}
		}
	}

	for ip, d := range config.Ping.IPs {

		if pIP := net.ParseIP(ip); pCIDR.Contains(pIP) {
			result = append(result, ip)
			continue
		}

		if alsoAutoGroups {
			found := false
			for _, g := range d.Groups {
				if autoGroups[g] {
					found = true
					break
				}
			}
			if found {
				result = append(result, ip)
			}
		}
	}

	return result, nil
}

func IsIpOnSlave(ip string, slave string, config ConfigInfo) bool {
	ipInfo, ok := config.Ping.IPs[ip]
	if !ok {
		return false
	}

	for _, s := range ipInfo.Slaves {
		if slave == s {
			return true
		}
	}

	return false
}
