package main

// this example add or removes ips from/to specific slaves list

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	url    = flag.String("url", "", "URL of cocopacket master instance")
	user   = flag.String("user", "", "username for authorization")
	passwd = flag.String("password", "", "password for authorization")
)

type setSAMLConfigPayload struct {
	SAMLMeta []byte `json:"samlMeta"`
	SAMLKey  []byte `json:"samlKey,omitempty"`
	SAMLCert []byte `json:"samlCert,omitempty"`
	BaseURL  string `json:"baseURL,omitempty"`
}

func main() {
	var err error

	flag.Parse()
	if *url == "" || len(flag.Args()) != 1 {
		fmt.Println("Usage: ", os.Args[0], "[flags] saml-meta.xml")
		flag.Usage()
		return
	}

	file, err := os.ReadFile(flag.Args()[0])
	if err != nil {
		log.Fatalln("Error reading file:", err)
	}

	if strings.HasSuffix(*url, "/") {
		*url = (*url)[:len(*url)-1]
	}

	api.Init(*url, *user, *passwd)

	var res api.APIResult
	err = api.Send("POST", *url+"/v1/config/saml", setSAMLConfigPayload{
		SAMLMeta: file,
		BaseURL:  *url,
	}, &res)
	if nil != err {
		log.Fatalln("Error setting SAML config (http):", err)
	}
	if res.Result != "OK" {
		log.Fatalln("Error setting SAML config:", res.Error, res.Result)
	}

	fmt.Println("OK")
}
