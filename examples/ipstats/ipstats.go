package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	mainurl   = flag.String("url", "", "URL of cocopacket master instance")
	user      = flag.String("user", "", "username for authorization")
	passwd    = flag.String("password", "", "password for authorization")
	lossLimit = flag.Int("lossLimit", 20, "minimal loss to interpret as incident")
	latDiff   = flag.Int("latLimit", 30, "minimal latency % difference")
)

func main() {
	flag.Parse()
	args := flag.Args()

	if *mainurl == "" || len(args) != 2 {
		fmt.Println("Usage: ", os.Args[0], "[flags] ip slave")
		flag.Usage()
		return
	}

	if *lossLimit < 1 || *lossLimit > 100 {
		log.Fatalln("limit can't be less then 1 or more then 100")
	}

	api.Init(*mainurl, *user, *passwd)

	// it's not public api, but very fast and suitable for this task :)
	data, err := api.GetAvgDump(args[0], args[1])

	if err != nil {
		log.Fatalln("Error reading data from master:", err)
	}

	hours := api.AvgMapToSlice(data.Hours)
	if len(hours) > 24 {
		// cocopacket may return more then 24 hours, just limit to 24
		hours = hours[len(hours)-24:]
	}

	// first calculate avg latency for all day
	var (
		allLatency  float32
		allLatCount int
	)

	for _, item := range hours {
		if item.Count == 0 {
			continue
		}
		allLatency += item.Latency
		allLatCount += item.Count - item.Loss
	}

	avgLatency := allLatency / float32(allLatCount)
	// for little bit cleaner code calculate minimal and maximal latency we accept as normal
	minLatency := avgLatency - (avgLatency * float32(*latDiff) / 100)
	maxLatency := avgLatency + (avgLatency * float32(*latDiff) / 100)

	fmt.Printf("Avg latency for 24h: %.2f ms\nHours out of limit %d%% (%.2f .. %.2f ms) or loss above %d%%:\n", avgLatency, *latDiff, minLatency, maxLatency, *lossLimit)

	// and just print hours that doesn't fullfill limits
	for _, item := range hours {
		if item.Count == 0 {
			continue
		}

		incidents := []string{}

		// if loss is more then specified
		if item.Loss > 0 && (item.Loss >= (item.Count * *lossLimit / 100)) {
			incidents = append(incidents, fmt.Sprintf("loss %.2f%%", float32(item.Loss)/float32(item.Count)*100))
		}

		// and if have abnormal latency
		if item.Latency > 0 {
			itemLatency := item.Latency / float32(item.Count-item.Loss)
			if itemLatency < minLatency || itemLatency > maxLatency {
				incidents = append(incidents, fmt.Sprintf("latency %.2f ms", itemLatency))
			}
		}

		// print only if we have something
		if len(incidents) == 0 {
			continue
		}
		fmt.Printf("  %v: %s\n", item.TS, strings.Join(incidents, " | "))
	}
}
