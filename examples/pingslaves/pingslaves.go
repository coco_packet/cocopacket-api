package main

/* this example adds slaves as ips to ping each other (using same uplink)
   to use it all slaves have to have name in format LOCATION-UPLINK like NYC-LEVEL3 (names like C-* are ignored)
   P.S.: this example show how to use raw ip adding */

import (
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	url     = flag.String("url", "", "URL of cocopacket master instance")
	user    = flag.String("user", "", "username for authorization")
	passwd  = flag.String("password", "", "password for authorization")
	group   = flag.String("group", "TRANSIT_PATHS", "to which group add ips")
	fav     = flag.Bool("favorite", false, "Set new added ips as favorite")
	remove  = flag.Bool("remove", true, "delete unknown (old) IPs from group")
	defAll  = flag.Bool("default-from-all", false, "ping default slaves from all other (not default only)")
	dryrun  = flag.Bool("dryrun", false, "just to show list what to add where, not actualy send to master")
	filter  = flag.String("filter", "", "regex to filter slave (names)")
	protect = flag.Bool("protect", false, "use \"foced\" flag for added IPs")
)

func main() {
	var err error

	flag.Parse()
	if *url == "" {
		fmt.Println("Usage: ", os.Args[0], "[flags] [ip[ ip[ ip...]]]")
		flag.Usage()
		return
	}

	var filterRE *regexp.Regexp
	if *filter != "" {
		filterRE, err = regexp.Compile(*filter)
		if err != nil {
			log.Fatalln("Invalid filter regex: ", err)
		}
	}

	api.Init(*url, *user, *passwd)

	slaves, err := api.GetSlavesSources()
	if nil != err {
		log.Fatalln("Error on slave list get:", err)
	}

	var config api.ConfigInfo
	if *remove { // need config info for old ips remove later
		config, err = api.GetConfigInfo()
		if err != nil {
			log.Fatalln("Error on config get:", err)
		}
	}

	uplinks := map[string][]string{}
	allSlaves := []string{}
	for slave := range slaves {
		if filterRE != nil && !filterRE.MatchString(slave) {
			continue
		}

		parts := strings.Split(slave, "-")
		uplink := parts[len(parts)-1]
		uplinks[uplink] = append(uplinks[uplink], strings.Join(parts[:len(parts)-1], "-"))
		allSlaves = append(allSlaves, slave)
	}

	ips := map[string]api.TestDesc{}

	for uplink, hosts := range uplinks {
		if len(hosts) > 1 {
			if *dryrun {
				fmt.Printf("%s: %+v\n", uplink, hosts)
			}
			for _, host := range hosts {
				// yes, it's better ways like append(s[:index], s[index+1:]...) exists, but this is just example :)
				xslaves := []string{}

				if !*defAll || uplink != "DEFAULT" {
					for _, s := range hosts {
						if host == s {
							continue
						}
						xslaves = append(xslaves, s+"-"+uplink)
					}
				} else {
					xslaves = allSlaves
				}

				if *dryrun {
					fmt.Printf("  %s (%s): %+v\n", host, slaves[host+"-"+uplink], xslaves)
				}

				// some slaves may not source have ip defined (unconnected, for example)
				if slaves[host+"-"+uplink] == "" {
					if *dryrun {
						fmt.Println(host+"-"+uplink, "doesn't have source IP defined!")
					}
					continue
				}

				ips[slaves[host+"-"+uplink]] = api.TestDesc{
					Slaves:      xslaves,
					Description: host,
					Groups:      []string{*group + "->" + uplink + "->"},
					Favorite:    *fav,
					Locked:      *protect,
				}
			}
		}
	}

	ips2remove := []string{}
	if *remove { // remove all unknown/old IPs from this group
		// first get config where all IPs are listed
		prefix := *group + "->"

		for ip, info := range config.Ping.IPs {
			groupFound := false
			for _, group := range info.Groups {
				if strings.HasPrefix(group, prefix) {
					groupFound = true
					break
				}
			}
			if !groupFound {
				continue
			}
			if _, ok := ips[ip]; !ok {
				ips2remove = append(ips2remove, ip)
				fmt.Printf("  * old ip remove: %s %+v\n", ip, info)
			}
		}
	}

	if *dryrun {
		return
	}

	err = api.AddIPsRawForce(ips, *protect)
	if nil != err {
		log.Fatalln("Error add ips call:", err)
		return
	}

	if len(ips2remove) > 0 {
		err = api.DeleteIPsForce(ips2remove, *protect)
		if nil != err {
			log.Fatalln("Error delete ips call:", err)
			return
		}
	}

	fmt.Println("OK")
}
