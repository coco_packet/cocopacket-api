package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	mainurl     = flag.String("url", "", "URL of cocopacket master instance")
	user        = flag.String("user", "", "username for authorization")
	passwd      = flag.String("password", "", "password for authorization")
	lossLimit   = flag.Int("lossLimit", 20, "minimal loss to interpret as incident")
	latDiff     = flag.Int("latLimit", 30, "minimal latency % difference")
	latMinChg   = flag.Int("latMinimalChange", 10, "minimal latency ms difference")
	latDecAlert = flag.Bool("latDecreaseAlert", false, "alert also in latency decrease not only increase")
	as          = flag.Int64("as", 0, "report for AS")
	asGroup     = flag.Bool("asGroup", true, "include AS auto-group for as report")
	network     = flag.String("network", "", "report for network (CIDR format)")
	netGroup    = flag.Bool("netGroup", true, "include network auto-group for as report")
	group       = flag.String("group", "", "report for specified group")
	iplist      = flag.String("ips", "", "report comma-separated list of IPs")
	slaves      = flag.String("slaves", "", "limit report on comma-separated slaves list")
	quorum      = flag.Int("quorum", 30, "percents of IPs triggered to report incident")
	ignoredead  = flag.Bool("ignoreDead", true, "ignore IPs with 100% loss for last 24 hours")
	incDump     = flag.Bool("dumpIncidents", false, "dump all incidents instead of quorum counting")
	jsonOut     = flag.Bool("json", false, "output in JSON format")
	lastHour    = flag.Bool("lastHour", false, "report incidents only on previous hour (for every-hour cron)")
	muteRepLat  = flag.Bool("muteRLat", false, "ignore repeating latency-based incident (report only the first one)")
	muteRepLoss = flag.Bool("muteRLoss", false, "ignore repeating loss-based incident (report only the first one)")
)

func fatalOnErr(err error, description string) {
	if err == nil {
		return
	}
	log.Fatalln(description, err)
}

type incident struct {
	Latency    float32 `json:"latency,omitempty"`
	AvgLatency float32 `json:"avglatency,omitempty"`
	Loss       float32 `json:"loss,omitempty"`
}

func main() {
	flag.Parse()

	if *mainurl == "" || (*as == 0 && *network == "" && *group == "" && *iplist == "") {
		flag.Usage()
		return
	}

	if *lossLimit < 1 || *lossLimit > 100 {
		log.Fatalln("limit can't be less then 1 or more then 100")
	}

	api.Init(*mainurl, *user, *passwd)

	config, err := api.GetConfigInfo()
	fatalOnErr(err, "Error loading config: ")

	limitToSlaves := map[string]bool{}
	all_slaves, err := api.GetSlavesIPs()
	fatalOnErr(err, "Error loading slaves list: ")

	if *slaves != "" {
		tmp := strings.Split(*slaves, ",")
		for _, slave := range tmp {
			slave = strings.TrimSpace(slave)
			if _, ok := all_slaves[slave]; ok {
				limitToSlaves[slave] = true
			} else {
				log.Println("Unknown slave ", slave)
			}
		}
	} else {
		for slave := range all_slaves {
			limitToSlaves[slave] = true
		}
	}

	ips := []string{}

	if *as > 0 {
		ips = api.GetIPsInAS(*as, *asGroup, config)
	} else if *network != "" {
		ips, err = api.GetIPsInCIDR(*network, *netGroup, config)
		fatalOnErr(err, "Loading IPs for CIDR: ")
	} else if *group != "" {
		ips = api.GetIPsInGroup(*group, config)
	} else if *iplist != "" {
		ips = strings.Split(*iplist, ",")
	}

	if len(ips) == 0 {
		log.Fatalln("List of IPs for report is empty")
	}

	// by default we have no limit on timestamp
	onlyTS := time.Time{}
	// but in case of "-lastHour=true" we need to select only previous hour for report
	if *lastHour {
		// just previous (completed) hour
		onlyTS = time.Now().Truncate(time.Hour).Add(-time.Hour)
	}

	//                 slave     time           ip     incident
	incidentMap := map[string]map[time.Time]map[string]incident{}
	// we may need to exclude dead ips from quorum also
	aliveCounts := map[string]bool{}

	// fill in incidentMap
	for slave := range limitToSlaves {
		incidentsOnSlave := map[time.Time]map[string]incident{}

		// now we're getting stats for last 24hours
		for _, ip := range ips {
			if !api.IsIpOnSlave(ip, slave, config) {
				continue
			}

			data, err := api.GetAvgDump(ip, slave)
			fatalOnErr(err, "Error loading avg data for "+ip+"@"+slave)

			hours := api.AvgMapToSlice(data.Hours)
			if len(hours) > 24 {
				// cocopacket may return more then 24 hours, just limit to 24
				hours = hours[len(hours)-24:]
			}

			// first calculate avg latency for all day
			var (
				allLatency  float32
				allLatCount int
			)

			for _, item := range hours {
				if item.Count == 0 {
					continue
				}
				allLatency += item.Latency
				allLatCount += item.Count - item.Loss
			}

			avgLatency := allLatency / float32(allLatCount)
			// for little bit cleaner code calculate minimal and maximal latency we accept as normal
			minLatency := avgLatency - (avgLatency * float32(*latDiff) / 100)
			if minLatency > (avgLatency - float32(*latMinChg)) {
				minLatency = avgLatency - float32(*latMinChg)
			}
			if !*latDecAlert {
				minLatency = 0
			}

			maxLatency := avgLatency + (avgLatency * float32(*latDiff) / 100)
			if maxLatency < (avgLatency + float32(*latMinChg)) {
				maxLatency = avgLatency + float32(*latMinChg)
			}

			var prevItem *api.AvgItem

			for _, item := range hours {
				// limit to last hour if selected
				if !onlyTS.IsZero() && item.TS != onlyTS {
					prevItem = item
					continue
				}

				if item.Count == 0 {
					prevItem = item
					continue
				}
				if *ignoredead && item.Count == item.Loss {
					prevItem = item
					continue
				}
				aliveCounts[ip] = true

				i := incident{}

				// if loss is more then specified
				if item.Loss > 0 && (item.Loss >= (item.Count * *lossLimit / 100)) {
					if !(*muteRepLoss && prevItem != nil && (prevItem.Loss > 0 && (prevItem.Loss >= (prevItem.Count * *lossLimit / 100)))) {
						i.Loss = float32(item.Loss) / float32(item.Count) * 100
					}
				}

				// and if have abnormal latency
				if item.Latency > 0 {
					itemLatency := item.Latency / float32(item.Count-item.Loss)
					if itemLatency < minLatency || itemLatency > maxLatency {
						if *muteRepLat && prevItem != nil {
							prevItemLatency := prevItem.Latency / float32(prevItem.Count-prevItem.Loss)
							if !(prevItemLatency < minLatency || prevItemLatency > maxLatency) {
								i.Latency = itemLatency
								i.AvgLatency = avgLatency
							}
						} else {
							i.Latency = itemLatency
							i.AvgLatency = avgLatency
						}
					}
				}

				if i.Loss > 0 || i.AvgLatency > 0 {
					// it's an incident! 🥳
					if incidentsOnSlave[item.TS] == nil {
						incidentsOnSlave[item.TS] = map[string]incident{}
					}
					incidentsOnSlave[item.TS][ip] = i
				}

				prevItem = item
			}

		}

		if len(incidentsOnSlave) > 0 {
			incidentMap[slave] = incidentsOnSlave
		}
	}

	quorumCount := *quorum * len(aliveCounts) / 100

	filteredIncidents := map[string]map[time.Time]map[string]incident{}
	if *incDump {
		// just output everything
		filteredIncidents = incidentMap
	} else {
		// filter only that things that have quorum

		for slave, slaveTimesamps := range incidentMap {
			for ts, data := range slaveTimesamps {
				if len(data) < quorumCount {
					continue
				}
				if filteredIncidents[slave] == nil {
					filteredIncidents[slave] = map[time.Time]map[string]incident{}
				}
				filteredIncidents[slave][ts] = data
			}
		}
	}

	// output either JSON or plain text
	if *jsonOut {

		outData, err := json.MarshalIndent(filteredIncidents, "", "  ")
		fatalOnErr(err, "Marshaling json error: ")
		fmt.Println(string(outData))

		return

	}

	for slave, slaveTimesamps := range filteredIncidents {
		fmt.Println("\n", slave)

		for ts, data := range slaveTimesamps {
			fmt.Println("\n  ", ts)

			for ip, incident := range data {
				i := []string{}
				if incident.Loss > 0 {
					i = append(i, fmt.Sprintf("loss: %.2f", incident.Loss))
				}
				if incident.AvgLatency > 0 {
					i = append(i, fmt.Sprintf("latency: %.2f (avg: %.2f)", incident.Latency, incident.AvgLatency))
				}

				fmt.Printf("    %s: %s\n", ip, strings.Join(i, " | "))
			}
		}
		fmt.Println("")
	}
}
