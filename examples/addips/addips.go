package main

// this example adds ips specified in command line (or stdin) to all slaves exist on master

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	url       = flag.String("url", "", "URL of cocopacket master instance")
	user      = flag.String("user", "", "username for authorization")
	passwd    = flag.String("password", "", "password for authorization")
	desc      = flag.String("description", "", "description setted for new added ips or leave empty to use ip itself as description")
	group     = flag.String("group", "auto", "to which group add ips")
	fav       = flag.Bool("favorite", false, "Set new added ips as favorite")
	stdin     = flag.Bool("stdin", false, "read ip list from stdin instead of command line")
	slaves    = flag.String("slaves", "", "comma separated list of slaves, all slaves if empty")
	locked    = flag.Bool("locked", false, "lock IP to prevent changes")
	force     = flag.Bool("force", false, "allow change of locked IPs")
	overwrite = flag.Bool("overwrite", false, "overwrite existing IPs")
)

func main() {
	var err error

	flag.Parse()
	if *url == "" {
		fmt.Println("Usage: ", os.Args[0], "[flags] [ip[ ip[ ip...]]]")
		flag.Usage()
		return
	}

	api.Init(*url, *user, *passwd)

	slaveList := strings.Split(*slaves, ",")
	if len(slaveList) == 0 || ((len(slaveList) == 1) && (slaveList[0] == "")) {
		slaveList, err = api.GetSlaveList()
		if nil != err {
			log.Fatalln("Error on slave list get:", err)
		}
	}

	var ips []string
	if *stdin {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			ips = append(ips, strings.TrimSpace(scanner.Text()))
		}
	} else {
		ips = flag.Args()
	}

	// get current IP list to avoid adding just existing ips
	existIPs, err := api.GetConfigInfo()
	if nil != err {
		log.Fatalln("Error reading current config from master:", err)
	}

	ipsToAdd := make([]string, 0, len(ips))
	if *overwrite {
		ipsToAdd = ips
	} else {
		for _, ip := range ips {
			if _, exist := existIPs.Ping.IPs[ip]; !exist {
				ipsToAdd = append(ipsToAdd, ip)
			}
		}
	}

	if len(ipsToAdd) == 0 {
		fmt.Println("No new IPs to add")
		return
	}

	err = api.AddIPs(ipsToAdd, slaveList, *desc, []string{*group + "->"},
		*fav, *locked, *force)
	if nil != err {
		log.Fatalln("Error add ips call:", err)
	}

	fmt.Println("OK")
}
