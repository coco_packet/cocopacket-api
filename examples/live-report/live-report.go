package main

// live report is something like morning-report, but working with minutes data instead of hour
// and designed to be starter, for example, every 5 minutes to get report of problems "live"

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"strings"
	"time"

	api "gitlab.com/coco_packet/cocopacket-api"
	"golang.org/x/exp/constraints"
)

var (
	mainurl   = flag.String("url", "", "URL of cocopacket master instance")
	user      = flag.String("user", "", "username for authorization")
	passwd    = flag.String("password", "", "password for authorization")
	lossLimit = flag.Int("lossLimit", 20, "minimal loss to interpret as incident")
	maxLoss   = flag.Int("maxLoss", 100, "maximum loss to interpret as incident (for example 99 to prevent reporting totaly dead destination)")
	latDiff   = flag.Int("latLimit", 50, "minimal latency % difference")
	latMinChg = flag.Int("latMinimalChange", 10, "minimal latency ms difference")

	as       = flag.Int64("as", 0, "report for AS")
	asGroup  = flag.Bool("asGroup", true, "include AS auto-group for as report")
	network  = flag.String("network", "", "report for network (CIDR format)")
	netGroup = flag.Bool("netGroup", true, "include network auto-group for as report")
	group    = flag.String("group", "", "report for specified group")
	iplist   = flag.String("ips", "", "report comma-separated list of IPs")
	slaves   = flag.String("slaves", "", "limit report on comma-separated slaves list")

	quorum  = flag.Int("quorum", 30, "percents of IPs triggered to report incident")
	incDump = flag.Bool("dumpIncidents", false, "dump all incidents instead of quorum counting")
	jsonOut = flag.Bool("json", false, "output in JSON format")

	lastMinutes = flag.Int("lastMinutes", 31, "inspect last X minutes")
	minDuration = flag.Int("minDuration", 10, "minumum incident duration")
	maxDuration = flag.Int("maxDuration", 19, "maximum incident duration (to prevent reapiting reports)")
	onlyActive  = flag.Bool("onlyActive", false, "report only unfinished incidents")

	minimalHours = flag.Int("minimalHours", 24, "if greater then zero require that IP has data for minimal X hours to prevent reporting fresh added (maximal value is 24!)")
)

func fatalOnErr(err error, description string) {
	if err == nil {
		return
	}
	log.Fatalln(description, err)
}

type incident struct {
	Latency     float32 `json:"latency,omitempty"`
	PrevLatency float32 `json:"prevlatency,omitempty"`
	Loss        float32 `json:"loss,omitempty"`
	Duration    int     `json:"duration"`
}

func filterSlice[T any](s []T, check func(T) bool) []T {
	result := make([]T, 0, len(s))

	for _, value := range s {
		if check(value) {
			result = append(result, value)
		}
	}

	return result
}

func max[T constraints.Ordered](a T, b T) T {
	if a > b {
		return a
	}
	return b
}

func main() {
	flag.Parse()

	if *mainurl == "" || (*as == 0 && *network == "" && *group == "" && *iplist == "") {
		flag.Usage()
		return
	}

	if *lossLimit < 1 || *lossLimit > 100 {
		log.Fatalln("limit can't be less then 1 or more then 100")
	}

	if *minimalHours < 1 {
		*minimalHours = 0
	}

	if *minimalHours > 24 {
		*minimalHours = 24
	}

	api.Init(*mainurl, *user, *passwd)

	config, err := api.GetConfigInfo()
	fatalOnErr(err, "Error loading config: ")

	limitToSlaves := map[string]bool{}
	all_slaves, err := api.GetSlavesIPs()
	fatalOnErr(err, "Error loading slaves list: ")

	if *slaves != "" {
		tmp := strings.Split(*slaves, ",")
		for _, slave := range tmp {
			slave = strings.TrimSpace(slave)
			if _, ok := all_slaves[slave]; ok {
				limitToSlaves[slave] = true
			} else {
				log.Println("Unknown slave ", slave)
			}
		}
	} else {
		for slave := range all_slaves {
			limitToSlaves[slave] = true
		}
	}

	ips := []string{}

	if *as > 0 {
		ips = api.GetIPsInAS(*as, *asGroup, config)
	} else if *network != "" {
		ips, err = api.GetIPsInCIDR(*network, *netGroup, config)
		fatalOnErr(err, "Loading IPs for CIDR: ")
	} else if *group != "" {
		ips = api.GetIPsInGroup(*group, config)
	} else if *iplist != "" {
		ips = strings.Split(*iplist, ",")
	}

	if len(ips) == 0 {
		log.Fatalln("List of IPs for report is empty")
	}

	//                 slave     time           ip     incident
	incidentMap := map[string]map[time.Time]map[string]incident{}

	minTS := time.Now().Add(-(time.Duration(*lastMinutes+1) * time.Minute))

	// fill in incidentMap
	for slave := range limitToSlaves {
		incidentsOnSlave := map[time.Time]map[string]incident{}

		for _, ip := range ips {
			if !api.IsIpOnSlave(ip, slave, config) {
				continue
			}

			data, err := api.GetAvgDump(ip, slave)
			fatalOnErr(err, "Error loading avg data for "+ip+"@"+slave)

			if *minimalHours > 0 && len(data.Hours) < *minimalHours {
				continue
			}

			minutes := api.AvgMapToSlice(data.Minutes)
			if len(minutes) > (*lastMinutes + 1) {
				// cocopacket may return more then X minutes, just limit to lastMinutes count
				minutes = minutes[len(minutes)-(*lastMinutes+1):]
			}

			// just filter data to have
			minutes = filterSlice(minutes, func(v *api.AvgItem) bool {
				return v.TS.After(minTS) && v.Count > 0
			})

			duration := 0
			preIncidentLatency := float32(0)
			preIncidentLatPct := float32(0)
			inc := incident{}

			// form 1 to have "prev item"
			for i, item := range minutes {
				if i == 0 {
					if item.Latency > 0 {
						preIncidentLatency = item.Latency / float32(item.Count-item.Loss)
						preIncidentLatPct = preIncidentLatency / 100
					}

					continue // skip first element
				}

				haveIncident := false

				if item.Loss > 0 {
					lossP := float32(item.Loss) / float32(item.Count) * 100

					if lossP >= float32(*lossLimit) && lossP <= float32(*maxLoss) {
						haveIncident = true
						inc.Loss = max(inc.Loss, lossP)
					}
				}

				thisLatency := float32(0)
				if item.Latency > 0 {
					thisLatency = item.Latency / float32(item.Count-item.Loss)
				}

				// yes, if in if, but it's easier to read :)
				if (item.Latency > 0) && (preIncidentLatency > 0) && (preIncidentLatPct > 0) {

					latChange := thisLatency - preIncidentLatency
					latPctChange := latChange / preIncidentLatPct

					if (latChange >= float32(*latMinChg)) && (latPctChange >= float32(*latDiff)) {
						haveIncident = true
						inc.Latency = max(inc.Latency, thisLatency)
						if inc.PrevLatency == 0 {
							inc.PrevLatency = preIncidentLatency
						}
					}
				}

				if haveIncident {
					duration++

					if i == (len(minutes)-1) && duration > *minDuration && duration <= *maxDuration {
						// it's last minute but we still have it :)
						if incidentsOnSlave[item.TS] == nil {
							incidentsOnSlave[item.TS] = map[string]incident{}
						}
						inc.Duration = duration
						incidentsOnSlave[item.TS][ip] = inc
					}
				} else {
					if duration > *minDuration && duration <= *maxDuration {
						if !*onlyActive {

							// actual incident add
							if incidentsOnSlave[item.TS] == nil {
								incidentsOnSlave[item.TS] = map[string]incident{}
							}
							inc.Duration = duration
							incidentsOnSlave[item.TS][ip] = inc
						}
					}
					duration = 0
					if item.Latency > 0 {
						preIncidentLatency = item.Latency / float32(item.Count-item.Loss)
						preIncidentLatPct = preIncidentLatency / 100
					}
				}
			}
			if len(incidentsOnSlave) > 0 {
				incidentMap[slave] = incidentsOnSlave
			}
		}
	}

	quorumCount := *quorum * len(ips) / 100

	filteredIncidents := map[string]map[time.Time]map[string]incident{}
	if *incDump {
		// just output everything
		filteredIncidents = incidentMap
	} else {
		// filter only that things that have quorum

		for slave, slaveTimesamps := range incidentMap {
			for ts, data := range slaveTimesamps {
				if len(data) < quorumCount {
					continue
				}
				if filteredIncidents[slave] == nil {
					filteredIncidents[slave] = map[time.Time]map[string]incident{}
				}
				filteredIncidents[slave][ts] = data
			}
		}
	}

	// output either JSON or plain text
	if *jsonOut {

		outData, err := json.MarshalIndent(filteredIncidents, "", "  ")
		fatalOnErr(err, "Marshaling json error: ")
		fmt.Println(string(outData))

		return

	}

	for slave, slaveTimesamps := range filteredIncidents {
		fmt.Println("\n", slave)

		for ts, data := range slaveTimesamps {
			fmt.Println("\n  ", ts)

			for ip, incident := range data {
				i := []string{}
				if incident.Loss > 0 {
					i = append(i, fmt.Sprintf("loss: %.2f%%", incident.Loss))
				}
				if incident.PrevLatency > 0 {
					i = append(i, fmt.Sprintf("latency: %.2f (prev: %.2f)", incident.Latency, incident.PrevLatency))
				}
				i = append(i, fmt.Sprintf("duration %d minutes", incident.Duration))

				fmt.Printf("    %s: %s\n", ip, strings.Join(i, " | "))
			}
		}
		fmt.Println("")
	}
}
