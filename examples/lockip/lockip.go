package main

// this example list ips from master

import (
	"flag"
	"fmt"
	"log"
	"os"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	url    = flag.String("url", "", "URL of cocopacket master instance")
	user   = flag.String("user", "", "username for authorization")
	passwd = flag.String("password", "", "password for authorization")
)

func main() {
	flag.Parse()
	ips := flag.Args()

	if *url == "" || len(ips) < 1 {
		fmt.Println("Usage: ", os.Args[0], "[flags] ip[ ip[ ip...]]")
		flag.Usage()
		return
	}

	api.Init(*url, *user, *passwd)

	config, err := api.GetConfigInfo()
	if nil != err {
		log.Fatalln("Error on ip list get:", err)
	}

	for _, ip := range ips {
		ipdesc, ok := config.Ping.IPs[ip]
		if !ok {
			fmt.Printf("ip %s doesn't exist\n", ip)
			continue
		}
		ipdesc.Locked = true
		err := api.AddIPsRaw(map[string]api.TestDesc{ip: ipdesc})
		if err != nil {
			fmt.Printf("  %s: %v\n", ip, err)
		} else {
			fmt.Printf("  %s: OK\n", ip)
		}
	}

}
