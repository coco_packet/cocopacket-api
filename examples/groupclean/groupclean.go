package main

// removes IPs from group depending on loss value (above treshold)

import (
	"flag"
	"fmt"
	"log"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	mainurl = flag.String("url", "", "URL of cocopacket master instance")
	user    = flag.String("user", "", "username for authorization")
	passwd  = flag.String("password", "", "password for authorization")

	lossThold = flag.Int("loss.treshold", 30, "total loss for last 24 hours to remove ip from group")
	group     = flag.String("group", "", "group to check")
	dryRun    = flag.Bool("dryrun", false, "only print IPs for remove, don't actually remove them")
)

func fatalOnErr(err error, description string) {
	if err == nil {
		return
	}
	log.Fatalln(description, err)
}

func main() {
	flag.Parse()

	if *mainurl == "" || *group == "" || *lossThold < 1 || *lossThold > 100 {
		flag.Usage()
		return
	}

	api.Init(*mainurl, *user, *passwd)

	config, err := api.GetConfigInfo()
	fatalOnErr(err, "Error loading config: ")

	ips := api.GetIPsInGroup(*group, config)

	if len(ips) == 0 {
		log.Fatalln("List of IPs in group is empty")
	}

	for _, ip := range ips {
		hasValidData := false
		for _, slave := range config.Ping.IPs[ip].Slaves {
			data, err := api.GetAvgDump(ip, slave)
			fatalOnErr(err, "Error loading avg data for "+ip+"@"+slave)

			totalCount := 0
			totalLoss := 0
			for _, hour := range data.Hours {
				totalCount += hour.Count
				totalLoss += hour.Loss
			}

			if totalLoss < (totalCount * *lossThold / 100) {
				hasValidData = true
				break
			}
		}

		if !hasValidData {
			fmt.Println("remove ", ip)
			if !*dryRun {
				err = api.DeleteIP(ip)
				fatalOnErr(err, "Error on ip ("+ip+") delete")
			}
		}
	}
}
