package main

// this example creates group with auto tracking pingable ips withing AS or network (cidr)

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	api "gitlab.com/coco_packet/cocopacket-api"
)

var (
	url     = flag.String("url", "", "URL of cocopacket master instance")
	user    = flag.String("user", "", "username for authorization")
	passwd  = flag.String("password", "", "password for authorization")
	group   = flag.String("group", "", "name of group (or name like AS12345 or 8.8.8.0_24 will be used)")
	slaves  = flag.String("slaves", "", "comma separated list of slaves, all slaves if empty")
	network = flag.String("network", "", "AS or CIDR (like \"AS12345\" or \"8.8.8.0/24\")")
	count   = flag.Int("ipCount", 10, "Count of pingable IPs to track in this network")
)

func main() {
	var err error

	flag.Parse()
	if *url == "" || *network == "" {
		fmt.Println("Usage: ", os.Args[0], "-group XXX -network YYY")
		flag.Usage()
		return
	}

	if *group == "" {
		*group = strings.ReplaceAll(*network, "/", "_")
	}

	api.Init(*url, *user, *passwd)

	slaveList := strings.Split(*slaves, ",")
	if len(slaveList) == 0 || ((len(slaveList) == 1) && (slaveList[0] == "")) {
		slaveList, err = api.GetSlaveList()
		if nil != err {
			log.Fatalln("Error on slave list get:", err)
		}
	}

	err = api.GroupSetSettings(*group, false /*public*/, true, /*autogroup*/
		*network, slaveList, *count,
		/*rest of parameteres are for notifications, don't use it in this example*/
		0, 0, 0, nil, nil)

	if nil != err {
		log.Fatalln("Error setting group settings:", err)
	}

	fmt.Println("OK")
}
