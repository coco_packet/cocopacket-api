# cocopacket-go-api
cocopacket.com API implementation implemented in golang

## api documentation
[api.md](api.md "API documentation")

## api examples
please look at examples folder - there are some usefull tools that are just prepared for usege covering basic functions like managing ips, users and so on

all examples has such flags:
* `-url` - link to your master instance like `http://yourname.client.cocopacket.com/`
* `-user` - login of user, usualy `admin`
* `-password` - just password

some examples has additional flags, just run to see

few examples:
```bash
./listips -url 'http://yourname.client.cocopacket.com/' -user admin -password helloWorld
./listslaves -url 'http://yourname.client.cocopacket.com/' -user admin -password helloWorld
./addslave -url 'http://yourname.client.cocopacket.com/' -user admin -password helloWorld 1.1.1.1 3030 NEWSLAVE
```

P.S.: in every examples folder just run `go build` to compile

## Docker - running CocoPacket-Probe
_The legacy term for the probe is `slave`._  

It is possible to run CocoPacket-Probe in a docker container.  
It requires basic knowledge about _Docker_ itself and port forwarding.

#### 1) Setup your docker file  

```docker
FROM scratch
ADD ca-certificates.crt /etc/ssl/certs/
ADD cocopacket-slave /
ADD slave.conf /
CMD ["/cocopacket-slave", "-config", "slave.conf"]
```

#### 2) Create a config file for your probe.

Create a `slave.conf` file with the following content
```
{
  "listen": "0.0.0.0:3030",
  "memory": 3600,
  "source": "0.0.0.0",
  "aesKey": "00112233445566778899AABBCCDDEEFF"
}
```
**!!! aesKey** must be changed to the key assigned to your CocoPacket instance.  
  
Your **aesKey** is accessible via the `add probe modal` in your client, simply download the initial probe config in `step 2` of the install instructions.

#### 3) Install `jq`
https://jqlang.github.io/jq/

This utility is a lightweight and flexible command-line JSON processor, used to parse JSON to get the latest CocoPacket-slave version.

#### 4) Download and Build CocoPacket-probe

```bash
cp /etc/ssl/certs/ca-certificates.crt . &&
curl cocopacket-slave "https://updates.cocopacket.cloud/cocopacket-slave/`curl -s https://updates.cocopacket.cloud/cocopacket-slave/linux-amd64.json  | jq -r .Version`/linux-amd64.gz" | gzip -dc > cocopacket-slave &&
chmod 755 cocopacket-slave &&
docker build -t cocoslave .
```

#### 5) Run probe
Run command example (adjust ports):  

_--publish [host_port]:[container_port]_  
_--name [name]_

```bash
docker run --publish 3030:3030 --name coco1 cocoslave

```
#### 6) Connect probe
Open your online CocoPacket environment (Client) and add connection to your probe through the `Add Probe` modal.